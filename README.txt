Note: compiling for Windows might need to change references to SDL. 
Use the template code for reference.





To fix time for Windows, do this in reverse:
https://edstem.org/au/courses/6697/discussion/640247





Don't worry about this

sed -i '' 's/SDL.h/SDL2\/SDL.h/g' {*.c,*.h} # for SDL.h
sed -i '' 's/SDL2_gfxPrimitives.h/SDL2\/SDL2_gfxPrimitives.h/g' {*.c,*.h} # for SDL2_gfxPrimitives.h
gcc wall.c formulas.c robot.c main.c -o main -lSDL2 

need to source path of pkg-config with:
linkpkg
(alias defined in .bashrc)
