#include "stdio.h"
#include "stdlib.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL2_gfxPrimitives.h"
//#include "time.h"
#include <sys/time.h>

#include "formulas.h"
#include "wall.h"
#include "robot.h"

int done = 0;


int main(int argc, char *argv[]) {
    SDL_Window *window;
    SDL_Renderer *renderer;

    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        return 1;
    }

    window = SDL_CreateWindow("Robot Maze", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, OVERALL_WINDOW_WIDTH, OVERALL_WINDOW_HEIGHT, SDL_WINDOW_OPENGL);
    window = SDL_CreateWindow("Robot Maze", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, OVERALL_WINDOW_WIDTH, OVERALL_WINDOW_HEIGHT, SDL_WINDOW_OPENGL);
    renderer = SDL_CreateRenderer(window, -1, 0);

    struct Robot robot;
    struct Wall_collection *head = NULL;
    int front_left_sensor, front_right_sensor, front_centre_sensor=0;
    // clock_t start_time, end_time;
    // int msec;
    struct timeval start_time, end_time;
    gettimeofday(&start_time, 0);
    unsigned long msec;

    // SETUP MAZE
    // You can create your own maze here. line of code is adding a wall.
    // You describe position of top left corner of wall (x, y), then width and height going down/to right
    // Relative positions are used (OVERALL_WINDOW_WIDTH and OVERALL_WINDOW_HEIGHT)
    // But you can use absolute positions. 10 is used as the width, but you can change this.

    // from Ed - Zhuo Chen
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2-320, OVERALL_WINDOW_HEIGHT/2-250, 10, OVERALL_WINDOW_HEIGHT/2+250); //left wall
    // insertAndSetFirstWall(&head, 2,  OVERALL_WINDOW_WIDTH/2-400, OVERALL_WINDOW_HEIGHT/2-240, 800, 10); //roof
    // insertAndSetFirstWall(&head, 3,  OVERALL_WINDOW_WIDTH/2+310, OVERALL_WINDOW_HEIGHT/2-250, 10, OVERALL_WINDOW_HEIGHT/2+100); //right wall

    // insertAndSetFirstWall(&head, 4,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2+50, 10, OVERALL_WINDOW_HEIGHT/2); //first lower wall
    // insertAndSetFirstWall(&head, 5,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2-170, 10, 150); //first upper wall
    // insertAndSetFirstWall(&head, 6,  OVERALL_WINDOW_WIDTH/2-240, OVERALL_WINDOW_HEIGHT/2+230, 800, 10); //floor
    // insertAndSetFirstWall(&head, 7,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2+50, 160, 10); //second horizontal lower wall
    // insertAndSetFirstWall(&head, 8,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2-30, 100, 10); //second horizontal middle wall
    // insertAndSetFirstWall(&head, 9,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2-180, 100, 10); //second horizontal upper wall

    // insertAndSetFirstWall(&head, 10,  OVERALL_WINDOW_WIDTH/2-160, OVERALL_WINDOW_HEIGHT/2+120, 10, OVERALL_WINDOW_HEIGHT/2); //third lower wall
    // insertAndSetFirstWall(&head, 11,  OVERALL_WINDOW_WIDTH/2-160, OVERALL_WINDOW_HEIGHT/2-70, 10, 40); //third middle wall
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2-160, OVERALL_WINDOW_HEIGHT/2-230, 10, 50); //third upper wall
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2-160, OVERALL_WINDOW_HEIGHT/2+170, 80, 10); //fourth horizontal lower wall
    // insertAndSetFirstWall(&head, 14,  OVERALL_WINDOW_WIDTH/2-100, OVERALL_WINDOW_HEIGHT/2-115, 10, OVERALL_WINDOW_HEIGHT/2-65); //fourth middle wall
    // insertAndSetFirstWall(&head, 15,  OVERALL_WINDOW_WIDTH/2-180, OVERALL_WINDOW_HEIGHT/2-120, 260, 10); //fourth horizontal upper wall
    // insertAndSetFirstWall(&head, 16,  OVERALL_WINDOW_WIDTH/2-60, OVERALL_WINDOW_HEIGHT/2-230, 10, 50); //fourth upper wall

    // insertAndSetFirstWall(&head, 17,  OVERALL_WINDOW_WIDTH/2-60, OVERALL_WINDOW_HEIGHT/2+120, 140, 10); //fifth horizontal lower wall
    // insertAndSetFirstWall(&head, 18,  OVERALL_WINDOW_WIDTH/2-20, OVERALL_WINDOW_HEIGHT/2-115, 10, OVERALL_WINDOW_HEIGHT/2-65); //fifth middle wall
    // insertAndSetFirstWall(&head, 19,  OVERALL_WINDOW_WIDTH/2-60, OVERALL_WINDOW_HEIGHT/2-180, 140, 10); //fifth horizontal upper wall
    // insertAndSetFirstWall(&head, 20,  OVERALL_WINDOW_WIDTH/2+70, OVERALL_WINDOW_HEIGHT/2-230, 10, 50); //sixth upper wall
    // insertAndSetFirstWall(&head, 21,  OVERALL_WINDOW_WIDTH/2+70, OVERALL_WINDOW_HEIGHT/2, 10, OVERALL_WINDOW_HEIGHT/2-65); //sixth middle wall

    // insertAndSetFirstWall(&head, 22,  OVERALL_WINDOW_WIDTH/2+150, OVERALL_WINDOW_HEIGHT/2+70, 10, OVERALL_WINDOW_HEIGHT/2-65); //seventh lower wall
    // insertAndSetFirstWall(&head, 23,  OVERALL_WINDOW_WIDTH/2+70, OVERALL_WINDOW_HEIGHT/2, 160, 10); //seventh horizontal middle wall
    // insertAndSetFirstWall(&head, 24,  OVERALL_WINDOW_WIDTH/2+150, OVERALL_WINDOW_HEIGHT/2-170, 10, OVERALL_WINDOW_HEIGHT/2-65); //seventh upper wall
    // insertAndSetFirstWall(&head, 25,  OVERALL_WINDOW_WIDTH/2+150, OVERALL_WINDOW_HEIGHT/2+60, 80, 10); //eighth horizontal middle wall
    // insertAndSetFirstWall(&head, 26,  OVERALL_WINDOW_WIDTH/2+220, OVERALL_WINDOW_HEIGHT/2-240, 10, OVERALL_WINDOW_HEIGHT/2); // eighth upper wall
    // insertAndSetFirstWall(&head, 27,  OVERALL_WINDOW_WIDTH/2+220, OVERALL_WINDOW_HEIGHT/2+120, 10, OVERALL_WINDOW_HEIGHT/2-65); //eighth horizontal middle wall


    // from Ed - Ehab Ahmed
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2, OVERALL_WINDOW_HEIGHT/2+150, 10, OVERALL_WINDOW_HEIGHT/2); //1
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2, OVERALL_WINDOW_HEIGHT/2+150, OVERALL_WINDOW_WIDTH/2-150, 10);//2
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2+170, OVERALL_WINDOW_HEIGHT/2+85, 10, OVERALL_WINDOW_HEIGHT/8+15);//3
    // insertAndSetFirstWall(&head, 12,  130, OVERALL_WINDOW_HEIGHT/2+85, OVERALL_WINDOW_WIDTH/2+40, 10);//4
    // insertAndSetFirstWall(&head, 1,  120, OVERALL_WINDOW_HEIGHT/2-25, 10, OVERALL_WINDOW_HEIGHT/4);//5
    // insertAndSetFirstWall(&head, 12,  120, OVERALL_WINDOW_HEIGHT/2-25, 65, 10);//6
    // insertAndSetFirstWall(&head, 1,  185, 165, 10, 60);//7
    // insertAndSetFirstWall(&head, 12,  185, 165, OVERALL_WINDOW_WIDTH/2-10, 10);//8
    // insertAndSetFirstWall(&head, 1,  490, 165, 10,OVERALL_WINDOW_HEIGHT/2-100 );//9
    // insertAndSetFirstWall(&head, 12,  490, 300, 150, 10);//10
    // insertAndSetFirstWall(&head, 12,  550, 245, 100, 10);//11
    // insertAndSetFirstWall(&head, 1,  550, 110, 10,OVERALL_WINDOW_HEIGHT/2-100 );//12
    // insertAndSetFirstWall(&head, 12,  360, 110, OVERALL_WINDOW_WIDTH/2-130, 10);//13
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2+30, 55, 10, 65);//14
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2-30, 55, 60, 10);//15
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2-30, 55, 10, 65);//16
    // insertAndSetFirstWall(&head, 12,  125, 110, OVERALL_WINDOW_WIDTH/2-150, 10);//17
    // insertAndSetFirstWall(&head, 1,  125, 110, 10, 65);//18
    // insertAndSetFirstWall(&head, 12,  60, OVERALL_WINDOW_HEIGHT/2-75, 65, 10);//19
    // insertAndSetFirstWall(&head, 1,  60, OVERALL_WINDOW_HEIGHT/2-75, 10, OVERALL_WINDOW_HEIGHT/3+75);//20
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2+150, OVERALL_WINDOW_WIDTH/4, 10);//21
    // insertAndSetFirstWall(&head, 2,  OVERALL_WINDOW_WIDTH/2-100, OVERALL_WINDOW_HEIGHT/2+150, 10, OVERALL_WINDOW_HEIGHT/2);//22

    // Owen's design - uses default config in robot.c
    // insertAndSetFirstWall(&head, 1,  320, 370, 10, 110);
    // insertAndSetFirstWall(&head, 2,  220, 370, 10, 110);
    // insertAndSetFirstWall(&head, 3,  130, 370, 100, 10);
    // insertAndSetFirstWall(&head, 4,  60, 50, 10, 250);
    // insertAndSetFirstWall(&head, 5,  220, 290, 160, 10);
    // insertAndSetFirstWall(&head, 6,  550, 370, 10, 110);
    // insertAndSetFirstWall(&head, 7,  150, 220, 10, 70);
    // insertAndSetFirstWall(&head, 8,  150, 290, 70,10);
    // insertAndSetFirstWall(&head, 9,  230, 220, 10, 70);
    // insertAndSetFirstWall(&head, 10,  230, 220, 10, 70);
    // insertAndSetFirstWall(&head, 11,  380, 220, 10, 80);
    // insertAndSetFirstWall(&head, 12,  460, 220, 10, 80);
    // insertAndSetFirstWall(&head, 13,  550, 220, 10, 80);
    // insertAndSetFirstWall(&head, 14,  630, 220, 10, 150);
    // insertAndSetFirstWall(&head, 15,  150, 140, 240, 10);
    // insertAndSetFirstWall(&head, 16,  470, 470, 170, 10);
    // insertAndSetFirstWall(&head, 17,  460, 140, 100, 10);
    // insertAndSetFirstWall(&head, 18,  550, 140, 10, 80);
    // insertAndSetFirstWall(&head, 19,  460, 70, 10, 80);
    // insertAndSetFirstWall(&head, 20,  390, 70, 10, 80);
    // insertAndSetFirstWall(&head, 21,  390, 60, 80, 10);

    // int i = 0;
    // while ( i < 35 ) {
    //     insertAndSetFirstWall(&head, 22, 60 + (i * 2),300 + (i * 2),10, 10);
    //     i++;
    // }

    // i = 0;
    // while ( i < 45 ) {
    //     insertAndSetFirstWall(&head, 23, 60 + (i * 2),50 + (i * 2),10, 10);
    //     i++;
    // }

    // i = 0;
    // while ( i < 45 ) {
    //     insertAndSetFirstWall(&head, 24, 235 + (i * 3.31),220 + (i * 1.6),10, 10);
    //     i++;
    // }

    // i = 0;
    // while ( i < 45 ) {
    //     insertAndSetFirstWall(&head, 25, 320 + (i * 3.31),370 + (i * 2.3),10, 10);
    //     i++;
    // }

    // i = 0;
    // while ( i < 45 ) {
    //     insertAndSetFirstWall(&head, 26, 550 + (i * 1.7),290 - (i * 1.58),10, 10);
    //     i++;
    // }

    // from Ed - Gabrielle Zhu
    // int gap_width = 50;
    // insertAndSetFirstWall(&head, 1,
    //                       OVERALL_WINDOW_WIDTH/2, OVERALL_WINDOW_HEIGHT/6,  // start x and y
    //                       WALL_WIDTH, gap_width);
    // insertAndSetFirstWall(&head, 2,
    //                       OVERALL_WINDOW_WIDTH/6, OVERALL_WINDOW_HEIGHT/6,
    //                       OVERALL_WINDOW_WIDTH/2 - OVERALL_WINDOW_WIDTH/6, WALL_WIDTH);
    // insertAndSetFirstWall(&head, 3,
    //                       OVERALL_WINDOW_WIDTH/6 + gap_width, OVERALL_WINDOW_HEIGHT/6 + gap_width,
    //                       OVERALL_WINDOW_WIDTH/2 - OVERALL_WINDOW_WIDTH/6 - gap_width + WALL_WIDTH, WALL_WIDTH);
    // insertAndSetFirstWall(&head, 4,
    //                       OVERALL_WINDOW_WIDTH/6 - WALL_WIDTH, OVERALL_WINDOW_HEIGHT/6,
    //                       WALL_WIDTH, 5*OVERALL_WINDOW_HEIGHT/6 + WALL_WIDTH);
    // insertAndSetFirstWall(&head, 5,
    //                       OVERALL_WINDOW_WIDTH/6 + gap_width, OVERALL_WINDOW_HEIGHT/6 + gap_width,
    //                       WALL_WIDTH, OVERALL_WINDOW_HEIGHT/3 - gap_width/2);
    // insertAndSetFirstWall(&head, 6,
    //                       OVERALL_WINDOW_WIDTH/6 + gap_width, OVERALL_WINDOW_HEIGHT/2 + gap_width/2 - WALL_WIDTH,
    //                       2* OVERALL_WINDOW_WIDTH/3, WALL_WIDTH);
    // insertAndSetFirstWall(&head, 7,
    //                       OVERALL_WINDOW_WIDTH/6 + gap_width, OVERALL_WINDOW_HEIGHT/2 + 3*gap_width/2,
    //                       2* OVERALL_WINDOW_WIDTH/3, WALL_WIDTH);
    // insertAndSetFirstWall(&head, 8,
    //                       OVERALL_WINDOW_WIDTH/6 + gap_width, OVERALL_WINDOW_HEIGHT/2 + 3*gap_width/2,
    //                       WALL_WIDTH, OVERALL_WINDOW_HEIGHT/2 - gap_width/2);

    // from Ed - Sumin Baik

    // int k = 0; // 1st diag from left
    // while ( k < 50 ) {
    // insertAndSetFirstWall(&head, 1,
    //                     OVERALL_WINDOW_WIDTH/7 + (k * 2),
    //                     OVERALL_WINDOW_HEIGHT/2 + (k * 2),
    //                     10, 10);
    //     k++;
    // } 

    // int l = 0; // 2nd diag from left
    // while ( l < 100 ) {
    // insertAndSetFirstWall(&head, 2,
    //                     OVERALL_WINDOW_WIDTH/2-230 + (l * 2),
    //                     OVERALL_WINDOW_HEIGHT/3.5 + (l * 2),
    //                     10, 10);
    //     l++;
    // } 

    // int m = 0; // 3 diag from left
    // while ( m < 400 ) {
    // insertAndSetFirstWall(&head, 3,
    //                     OVERALL_WINDOW_WIDTH/2-120 + (m * 2),
    //                     OVERALL_WINDOW_HEIGHT/3.5 + (m * 2),
    //                     10, 10); 
    //     m++;
    // }  
    // insertAndSetFirstWall(&head, 4,  OVERALL_WINDOW_WIDTH/2-320, OVERALL_WINDOW_HEIGHT/2-250, 10, OVERALL_WINDOW_HEIGHT/2+250); //leftwall
    // insertAndSetFirstWall(&head, 5,  OVERALL_WINDOW_WIDTH/2+310, OVERALL_WINDOW_HEIGHT/2-160, 10, OVERALL_WINDOW_HEIGHT/2+250); //rightwall with gap
    // insertAndSetFirstWall(&head, 6,  OVERALL_WINDOW_WIDTH/2-400, OVERALL_WINDOW_HEIGHT/2+230, 800, 10); //floor
    // insertAndSetFirstWall(&head, 7,  OVERALL_WINDOW_WIDTH/2-400, OVERALL_WINDOW_HEIGHT/2-240, 800, 10); //roof

    // insertAndSetFirstWall(&head, 8,  OVERALL_WINDOW_WIDTH/7, OVERALL_WINDOW_HEIGHT/2, 10, OVERALL_WINDOW_HEIGHT/2); //first wall lower
    // insertAndSetFirstWall(&head, 9,  0, OVERALL_WINDOW_HEIGHT/2-103, 100, 10); //first horizontal

    // insertAndSetFirstWall(&head, 10,  OVERALL_WINDOW_WIDTH/(7*0.5)+7, OVERALL_WINDOW_HEIGHT/2+107, 10, OVERALL_WINDOW_HEIGHT/2); //second wall lower

    // insertAndSetFirstWall(&head, 11,  OVERALL_WINDOW_WIDTH/2.2, OVERALL_WINDOW_HEIGHT/2+180, 10, OVERALL_WINDOW_HEIGHT/2); //third wall lower

    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/7, OVERALL_WINDOW_HEIGHT/2-175, 370, 10); //long horizontal (closest to roof)
    // insertAndSetFirstWall(&head, 13,  200, OVERALL_WINDOW_HEIGHT/2-103, 200, 10); //the horizontal under ^ at end of 3rd diag

    // insertAndSetFirstWall(&head, 14,  OVERALL_WINDOW_WIDTH/1.2, OVERALL_WINDOW_HEIGHT/8-100, 10, OVERALL_WINDOW_HEIGHT/1.25); //vertical down wall (fourth wall upper)
    // insertAndSetFirstWall(&head, 15,  OVERALL_WINDOW_WIDTH/1.45, OVERALL_WINDOW_HEIGHT/2-50, 100, 10); //shorter horizontal in the middle of 3rd diag
    // insertAndSetFirstWall(&head, 16,  320, OVERALL_WINDOW_HEIGHT/2+15, 150, 10); //horizontal wall stemming from middle of vertical down
    // insertAndSetFirstWall(&head, 17,  OVERALL_WINDOW_WIDTH/1.2, OVERALL_WINDOW_HEIGHT/1.666, 50, 10); //tiny obstacle out horizontal near exit
    // insertAndSetFirstWall(&head, 18,  OVERALL_WINDOW_WIDTH/1.3, OVERALL_WINDOW_HEIGHT/1.4, 50, 10); //tiny stem horizontal end of vertical


    // from Ed - Justin Klasse
    // NOT PASSED - clips the wall
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2 - 200, OVERALL_WINDOW_HEIGHT/2 + 100, 10, 500);
    // insertAndSetFirstWall(&head, 2,  OVERALL_WINDOW_WIDTH/2 - 100, OVERALL_WINDOW_HEIGHT/2, 10, OVERALL_WINDOW_HEIGHT/2);
    // insertAndSetFirstWall(&head, 3,  OVERALL_WINDOW_WIDTH/2-225, OVERALL_WINDOW_HEIGHT/2, 125, 10);
    // insertAndSetFirstWall(&head, 4,  OVERALL_WINDOW_WIDTH/2-300, OVERALL_WINDOW_HEIGHT/2 + 100, 150, 10);
    // insertAndSetFirstWall(&head, 5,  OVERALL_WINDOW_WIDTH/2 - 300, OVERALL_WINDOW_HEIGHT/2 - 210, 10, OVERALL_WINDOW_HEIGHT/2 + 75);
    // insertAndSetFirstWall(&head, 6,  OVERALL_WINDOW_WIDTH/2-300, OVERALL_WINDOW_HEIGHT/2 - 210, 500, 10);
    // insertAndSetFirstWall(&head, 7,  OVERALL_WINDOW_WIDTH/2 - 220, OVERALL_WINDOW_HEIGHT/2 - 210, 10, OVERALL_WINDOW_HEIGHT/2 - 100);
    // insertAndSetFirstWall(&head, 8,  OVERALL_WINDOW_WIDTH/2 - 160, OVERALL_WINDOW_HEIGHT/2 - 100, 10, OVERALL_WINDOW_HEIGHT/2 - 175);
    // insertAndSetFirstWall(&head, 9,  OVERALL_WINDOW_WIDTH/2 - 100, OVERALL_WINDOW_HEIGHT/2 - 150, 10, OVERALL_WINDOW_HEIGHT/2 + 20);
    // insertAndSetFirstWall(&head, 10,  OVERALL_WINDOW_WIDTH/2 + 50, OVERALL_WINDOW_HEIGHT/2 - 210, 10, OVERALL_WINDOW_HEIGHT/2 - 100);
    // insertAndSetFirstWall(&head, 11,  OVERALL_WINDOW_WIDTH/2 - 50, OVERALL_WINDOW_HEIGHT/2 - 150, 50, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 10, OVERALL_WINDOW_HEIGHT/2 - 75, 50, 10);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2, OVERALL_WINDOW_HEIGHT/2 - 150, 10, OVERALL_WINDOW_HEIGHT/2 - 155);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 - 50, OVERALL_WINDOW_HEIGHT/2 - 75, 50, 10);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 - 50, OVERALL_WINDOW_HEIGHT/2 - 75, 10, OVERALL_WINDOW_HEIGHT/2 - 100);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 - 50, OVERALL_WINDOW_HEIGHT/2 + 60, 150, 10);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 - 25, OVERALL_WINDOW_HEIGHT/2 + 125, 10, OVERALL_WINDOW_HEIGHT/2 - 180);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 + 25, OVERALL_WINDOW_HEIGHT/2 + 100, 10, OVERALL_WINDOW_HEIGHT/2 - 220);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 + 25, OVERALL_WINDOW_HEIGHT/2 + 180, 10, OVERALL_WINDOW_HEIGHT/2 - 220);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 - 100, OVERALL_WINDOW_HEIGHT/2 + 230, 450, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 200, OVERALL_WINDOW_HEIGHT/2 + 150, 150, 10);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 + 100, OVERALL_WINDOW_HEIGHT/2, 10, OVERALL_WINDOW_HEIGHT/2 - 125);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 + 200, OVERALL_WINDOW_HEIGHT/2 - 210, 10, OVERALL_WINDOW_HEIGHT/2 + 125);
    // insertAndSetFirstWall(&head, 13,  OVERALL_WINDOW_WIDTH/2 + 25, OVERALL_WINDOW_HEIGHT/2 - 20, 10, OVERALL_WINDOW_HEIGHT/2 - 200);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 125, OVERALL_WINDOW_HEIGHT/2 - 100, 80, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 175, OVERALL_WINDOW_HEIGHT/2, 30, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 100, OVERALL_WINDOW_HEIGHT/2 + 75, 30, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 175, OVERALL_WINDOW_HEIGHT/2 + 150, 30, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2 + 110, OVERALL_WINDOW_HEIGHT/2 - 150, 30, 10);

    // from Ed - Sulav Malla
    // uses default in robot.c i guess?
    // they didn't set the endpoint but it passes
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH - 10, 0, 10, OVERALL_WINDOW_HEIGHT/2);
    // insertAndSetFirstWall(&head, 2,  OVERALL_WINDOW_WIDTH - 10,OVERALL_WINDOW_HEIGHT/2 + 50 , 10, OVERALL_WINDOW_HEIGHT/2 - 50);
    // insertAndSetFirstWall(&head, 3,  0, 0 , OVERALL_WINDOW_WIDTH, 10);
    // insertAndSetFirstWall(&head, 4,  0, OVERALL_WINDOW_HEIGHT - 10 , OVERALL_WINDOW_WIDTH, 10);
    // insertAndSetFirstWall(&head, 5,  0, 0 , 10, OVERALL_WINDOW_WIDTH );
    // insertAndSetFirstWall(&head, 6,  OVERALL_WINDOW_WIDTH - 100, 0, 10, OVERALL_WINDOW_HEIGHT/2 + 50);
    // insertAndSetFirstWall(&head, 7,  OVERALL_WINDOW_WIDTH - 100,OVERALL_WINDOW_HEIGHT/2 + 100 , 10, OVERALL_WINDOW_HEIGHT/2  - 100);
    // insertAndSetFirstWall(&head, 8,  OVERALL_WINDOW_WIDTH - 200, 0, 10, OVERALL_WINDOW_HEIGHT/2 + -50);
    // insertAndSetFirstWall(&head, 9,  OVERALL_WINDOW_WIDTH - 200,OVERALL_WINDOW_HEIGHT/2 , 10, OVERALL_WINDOW_HEIGHT/2 );
    // insertAndSetFirstWall(&head, 10,  OVERALL_WINDOW_WIDTH - 300, 0, 10, OVERALL_WINDOW_HEIGHT - 100);
    // insertAndSetFirstWall(&head, 11,  200, OVERALL_WINDOW_HEIGHT - 380 , 10, 380);

    // Gabriella's Maze
    // insertAndSetFirstWall(&head, 1, OVERALL_WINDOW_WIDTH/4, OVERALL_WINDOW_HEIGHT/2 + 230,  OVERALL_WINDOW_WIDTH, 10); //H
    // insertAndSetFirstWall(&head, 2, 0, OVERALL_WINDOW_HEIGHT/2 - 300, 10, OVERALL_WINDOW_HEIGHT/2 + 300); //V
    // insertAndSetFirstWall(&head, 3, OVERALL_WINDOW_WIDTH/4, OVERALL_WINDOW_HEIGHT/2+100, 10, 65); //V
    // insertAndSetFirstWall(&head, 4, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2+100, 10, 65); //V
    // insertAndSetFirstWall(&head, 5, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2+100, 80, 10); //H
    // insertAndSetFirstWall(&head, 6, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2+165, 90, 10); //H
    // insertAndSetFirstWall(&head, 7, OVERALL_WINDOW_WIDTH/4 + 80, OVERALL_WINDOW_HEIGHT/2, 10, 40); //V
    // insertAndSetFirstWall(&head, 8, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2, 10, 40); //V
    // insertAndSetFirstWall(&head, 9, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2, 160, 10); //H
    // insertAndSetFirstWall(&head, 10, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2 + 40, 170, 10); //H
    // insertAndSetFirstWall(&head, 11, OVERALL_WINDOW_WIDTH/4 + 80, OVERALL_WINDOW_HEIGHT/2+100, 10, 145); //V
    // insertAndSetFirstWall(&head, 12, OVERALL_WINDOW_WIDTH/4 + 80, OVERALL_WINDOW_HEIGHT/2+100, 80, 10); //H
    // insertAndSetFirstWall(&head, 13, OVERALL_WINDOW_WIDTH/4 + 160, OVERALL_WINDOW_HEIGHT/2 - 50, 10, OVERALL_WINDOW_HEIGHT/2 - 20); //V
    // insertAndSetFirstWall(&head, 14, OVERALL_WINDOW_WIDTH/4 - 80,OVERALL_WINDOW_HEIGHT/2 - 60, 250, 10); //H
    // insertAndSetFirstWall(&head, 15, 0, OVERALL_WINDOW_HEIGHT/2 - 120, 390, 10); //H
    // insertAndSetFirstWall(&head, 16, OVERALL_WINDOW_WIDTH/4 - 80, OVERALL_WINDOW_HEIGHT/2 - 185, 480, 10); //H
    // insertAndSetFirstWall(&head, 17, 0, 0, OVERALL_WINDOW_WIDTH, 10); //H
    // insertAndSetFirstWall(&head, 18, 390, OVERALL_WINDOW_HEIGHT/2 - 120, 10, OVERALL_WINDOW_HEIGHT/2 + 50); //V
    // insertAndSetFirstWall(&head, 19, 470, OVERALL_WINDOW_HEIGHT/2 - 60, 100, 10); //H
    // insertAndSetFirstWall(&head, 20, 470, OVERALL_WINDOW_HEIGHT/2 - 120, 100, 10); //H
    // insertAndSetFirstWall(&head, 21, 470, OVERALL_WINDOW_HEIGHT/2 - 110, 10, 50); //V
    // insertAndSetFirstWall(&head, 22, 560, OVERALL_WINDOW_HEIGHT/2 - 110, 10, 50); //V
    // insertAndSetFirstWall(&head, 23, 470, OVERALL_WINDOW_HEIGHT/2 + 20, 10, 150); //V
    // insertAndSetFirstWall(&head, 24, OVERALL_WINDOW_WIDTH - 10, 0, 10, 260); //V
    // insertAndSetFirstWall(&head, 25, OVERALL_WINDOW_WIDTH/2+230, OVERALL_WINDOW_HEIGHT/2+20, 10, 80); //V
    // insertAndSetFirstWall(&head, 26, OVERALL_WINDOW_WIDTH - 10, OVERALL_WINDOW_HEIGHT - 80, 10, 80); //V
    // insertAndSetFirstWall(&head, 27,  OVERALL_WINDOW_WIDTH/2+150, OVERALL_WINDOW_HEIGHT/2+160, OVERALL_WINDOW_WIDTH/2-100, 10); //H
    // insertAndSetFirstWall(&head, 28,  OVERALL_WINDOW_WIDTH/2+230, OVERALL_WINDOW_HEIGHT/2+100, OVERALL_WINDOW_WIDTH/2-100, 10); //H
    // insertAndSetFirstWall(&head, 29,  OVERALL_WINDOW_WIDTH/2+230, OVERALL_WINDOW_HEIGHT/2+20, OVERALL_WINDOW_WIDTH/2-100, 10); //H
    // insertAndSetFirstWall(&head, 30, 0, OVERALL_WINDOW_HEIGHT/2 + 230,  100, 10); //H

    // from Ed - Timothy Huxley "Open World"

    // /* Outside border */
    //     insertAndSetFirstWall(&head, 2,  0, 0, 320, 10);         /*broken line on top*/
    //     insertAndSetFirstWall(&head, 2,  370, 0, 290, 10);
    //     insertAndSetFirstWall(&head, 3,  0, 0, 10, 480);    /*left side*/
    //     insertAndSetFirstWall(&head, 4,  630, 0, 10, 480);  /*right side*/
    //     insertAndSetFirstWall(&head, 5,  0, 470, 640, 10);  //bottom
    //   /* Map */
    //     insertAndSetFirstWall(&head, 2,  320, 0, 10, 90);
    //     insertAndSetFirstWall(&head, 2,  370, 0, 10, 30);
    //     insertAndSetFirstWall(&head, 2,  320, 90, 50, 10);
    //   /* Map */
    //     insertAndSetFirstWall(&head, 2,  320, 0, 10, 90);
    //     insertAndSetFirstWall(&head, 2,  370, 0, 10, 30);
    //     insertAndSetFirstWall(&head, 2,  320, 90, 50, 10);



    // RACES

    // MAZE 1 AND 2
    // insertAndSetFirstWall(&head, 2,  220, 400, 10, 80);
    // insertAndSetFirstWall(&head, 2,  20, 400, 200, 10);
    // insertAndSetFirstWall(&head, 2,  20, 50, 10, 350);
    // insertAndSetFirstWall(&head, 2,  20, 50, 280, 10);
    // insertAndSetFirstWall(&head, 2,  300, 50, 10, 100);
    // insertAndSetFirstWall(&head, 2,  300, 150, 110, 10);
    // insertAndSetFirstWall(&head, 2,  400, 50, 10, 100);
    // insertAndSetFirstWall(&head, 2,  400, 50, 220, 10);
    // insertAndSetFirstWall(&head, 2,  620, 50, 10, 290);
    // insertAndSetFirstWall(&head, 2,  620, 340, 20, 10);

    // insertAndSetFirstWall(&head, 1,  320, 300, 10, 180);
    // insertAndSetFirstWall(&head, 2,  120, 300, 200, 10);
    // insertAndSetFirstWall(&head, 2,  120, 150, 10, 150);
    // insertAndSetFirstWall(&head, 2,  120, 150, 80, 10);
    // insertAndSetFirstWall(&head, 2,  200, 150, 10, 100);
    // insertAndSetFirstWall(&head, 2,  200, 250, 310, 10);
    // insertAndSetFirstWall(&head, 2,  500, 150, 10, 100);
    // insertAndSetFirstWall(&head, 2,  500, 150, 10, 100);
    // insertAndSetFirstWall(&head, 2,  500, 150, 20, 10);
    // insertAndSetFirstWall(&head, 2,  520, 150, 10, 290);
    // insertAndSetFirstWall(&head, 2,  520, 440, 120, 10);   

    // MAZE 3 AND 4

    // insertAndSetFirstWall(&head, 2,  640-10-220, 400, 10, 80);
    // insertAndSetFirstWall(&head, 2,  640-200-20, 400, 200, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-20, 50, 10, 350);
    // insertAndSetFirstWall(&head, 2,  640-280-20, 50, 280, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-300, 50, 10, 100);
    // insertAndSetFirstWall(&head, 2,  640-110-300, 150, 110, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-400, 50, 10, 100);
    // insertAndSetFirstWall(&head, 2,  640-400-220, 50, 220, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-620, 50, 10, 290);
    // insertAndSetFirstWall(&head, 2,  640-620-20, 340, 20, 10);

    // insertAndSetFirstWall(&head, 1,  640-10-320, 300, 10, 180);
    // insertAndSetFirstWall(&head, 2,  640-200-120, 300, 200, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-120, 150, 10, 150);
    // insertAndSetFirstWall(&head, 2,  640-80-120, 150, 80, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-200, 150, 10, 100);
    // insertAndSetFirstWall(&head, 2,  640-310-200, 250, 310, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-500, 150, 10, 100);
    // insertAndSetFirstWall(&head, 2,  640-20-500, 150, 20, 10);
    // insertAndSetFirstWall(&head, 2,  640-10-520, 150, 10, 290);
    // insertAndSetFirstWall(&head, 2,  640-120-520, 440, 120, 10);

    // MAZE 5 AND 6

    // #include "math.h"   

    // int i;
    // insertAndSetFirstWall(&head, 12,  120, 450, 10, 30);
    // insertAndSetFirstWall(&head, 12,  220, 450, 10, 30);
    // for (i = 0; i < 100; i++){
    //     insertAndSetFirstWall(&head, i,  20 + i , 350 + i, 10, 10); //1
    //     insertAndSetFirstWall(&head, i,  20 +100 + i , 350 + i, 10, 10); //1
    // }
    // insertAndSetFirstWall(&head, 12,  20, 280, 10, 70);
    // insertAndSetFirstWall(&head, 12,  120, 280, 10, 70);
    // for (i = 0; i < 180; i++){
    //     insertAndSetFirstWall(&head, i,  20 +190 - i/2 , 100 + i, 10, 10); //1
    // }
    // for (i = 0; i < 105; i++){
    //     insertAndSetFirstWall(&head, i,  20 +105/2 - i/2 , 175 + i, 10, 10); //1
    // }
    // insertAndSetFirstWall(&head, 2,  20, 175, 105/2, 10);
    // insertAndSetFirstWall(&head, 2,  20, 20, 10, 155);
    // insertAndSetFirstWall(&head, 2,  20, 20, 300, 10);
    // insertAndSetFirstWall(&head, 2,  320, 20, 10, 60);
    // insertAndSetFirstWall(&head, 2,  80, 100, 130, 10);
    // insertAndSetFirstWall(&head, 2,  80, 80, 10, 20);
    // insertAndSetFirstWall(&head, 2,  80, 80, 160, 10);

    // double j;
    // for (i = 0; i < 50; i++){
    //     j = i;
    //     insertAndSetFirstWall(&head, i+1,
    //                           // the most important bit is below.
    //                           // increase the 20 for a tighter bend
    //                           // descrease for a more meandering flow
    //                           320 + 30*sin(10*j * M_PI/180),
    //                           // increase the 5 for a spacier curve
    //                           (i * 5)+80,
    //                           10, 10);
    // }
    // for (i = 0; i < 75; i++){
    //     j = i;
    //     insertAndSetFirstWall(&head, i+1,
    //                           // the most important bit is below.
    //                           // increase the 20 for a tighter bend
    //                           // descrease for a more meandering flow
    //                           240 + 30*sin(10*j * M_PI/180),
    //                           // increase the 5 for a spacier curve
    //                           (i * 5)+80,
    //                           10, 10);
    // }
    // insertAndSetFirstWall(&head, 2,  345, 330, 105, 10);
    // insertAndSetFirstWall(&head, 2,  450, 190, 10, 150);
    // insertAndSetFirstWall(&head, 2,  380, 190, 70, 10);
    // insertAndSetFirstWall(&head, 2,  380, 20, 10, 170);
    // insertAndSetFirstWall(&head, 2,  380, 20, 260, 10);

    // insertAndSetFirstWall(&head, 2,  255, 455, 345, 10);
    // insertAndSetFirstWall(&head, 2,  600, 100, 10, 365);
    // insertAndSetFirstWall(&head, 2,  530, 100, 70, 10);
    // insertAndSetFirstWall(&head, 2,  530, 80, 10, 20);
    // insertAndSetFirstWall(&head, 2,  530, 80, 110, 10);


    // MAZE 7 AND 8

    #include "math.h"
    int i;
    insertAndSetFirstWall(&head, 12,  640-10-120, 450, 10, 30);
    insertAndSetFirstWall(&head, 12,  640-10-220, 450, 10, 30);

    for (i = 0; i < 100; i++){
        insertAndSetFirstWall(&head, i,  640-10-(20 + i) , 350 + i, 10, 10); //1
        insertAndSetFirstWall(&head, i,  640-10-(20 +100 + i) , 350 + i, 10, 10); //1
    }
    insertAndSetFirstWall(&head, 12,  640-10-20, 280, 10, 70);
    insertAndSetFirstWall(&head, 12,  640-10-120, 280, 10, 70);

    for (i = 0; i < 180; i++){
        insertAndSetFirstWall(&head, i,  640-10-(20 +190 - i/2) , 100 + i, 10, 10); //1
    }
    for (i = 0; i < 105; i++){
        insertAndSetFirstWall(&head, i,  640-10-(20 +105/2 - i/2) , 175 + i, 10, 10); //1
    }


    insertAndSetFirstWall(&head, 2,  640-105/2-20, 175, 105/2, 10);
    insertAndSetFirstWall(&head, 2,  640-10-20, 20, 10, 155);
    insertAndSetFirstWall(&head, 2,  640-300-20, 20, 300, 10);
    insertAndSetFirstWall(&head, 2,  640-10-320, 20, 10, 60);
    insertAndSetFirstWall(&head, 2,  640-130-80, 100, 130, 10);
    insertAndSetFirstWall(&head, 2,  640-10-80, 80, 10, 20);
    insertAndSetFirstWall(&head, 2,  640-160-80, 80, 160, 10);


    double j;
    for (i = 0; i < 50; i++){
        j = i;
        insertAndSetFirstWall(&head, i+1,
                              // the most important bit is below.
                              // increase the 20 for a tighter bend
                              // descrease for a more meandering flow
                              640-10-(320 + 30*sin(10*j * M_PI/180)),
                              // increase the 5 for a spacier curve
                              (i * 5)+80,
                              10, 10);
    }
    for (i = 0; i < 75; i++){
        j = i;
        insertAndSetFirstWall(&head, i+1,
                              // the most important bit is below.
                              // increase the 20 for a tighter bend
                              // descrease for a more meandering flow
                              640-10-(240 + 30*sin(10*j * M_PI/180)),
                              // increase the 5 for a spacier curve
                              (i * 5)+80,
                              10, 10);
    }

    insertAndSetFirstWall(&head, 2,  640-105-345, 330, 105, 10);
    insertAndSetFirstWall(&head, 2,  640-10-450, 190, 10, 150);
    insertAndSetFirstWall(&head, 2,  640-70-380, 190, 70, 10);
    insertAndSetFirstWall(&head, 2,  640-10-380, 20, 10, 170);
    insertAndSetFirstWall(&head, 2,  640-260-380, 20, 260, 10);

    insertAndSetFirstWall(&head, 2,  640-345-255, 455, 345, 10);
    insertAndSetFirstWall(&head, 2,  640-10-600, 100, 10, 365);
    insertAndSetFirstWall(&head, 2,  640-70-530, 100, 70, 10);
    insertAndSetFirstWall(&head, 2,  640-10-530, 80, 10, 20);
    insertAndSetFirstWall(&head, 2,  640-110-530, 80, 110, 10);


    // DEFAULT MAZE
    
    // insertAndSetFirstWall(&head, 1,  OVERALL_WINDOW_WIDTH/2, OVERALL_WINDOW_HEIGHT/2, 10, OVERALL_WINDOW_HEIGHT/2);
    // insertAndSetFirstWall(&head, 2,  OVERALL_WINDOW_WIDTH/2-100, OVERALL_WINDOW_HEIGHT/2+100, 10, OVERALL_WINDOW_HEIGHT/2-100);
    // insertAndSetFirstWall(&head, 3,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2+100, 150, 10);
    // insertAndSetFirstWall(&head, 4,  OVERALL_WINDOW_WIDTH/2-150, OVERALL_WINDOW_HEIGHT/2, 150, 10);
    // insertAndSetFirstWall(&head, 5,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2-200, 10, 300);
    // insertAndSetFirstWall(&head, 6,  OVERALL_WINDOW_WIDTH/2-150, OVERALL_WINDOW_HEIGHT/2-100, 10, 100);
    // insertAndSetFirstWall(&head, 7,  OVERALL_WINDOW_WIDTH/2-250, OVERALL_WINDOW_HEIGHT/2-200, 450, 10);
    // insertAndSetFirstWall(&head, 8,  OVERALL_WINDOW_WIDTH/2-150, OVERALL_WINDOW_HEIGHT/2-100, 250, 10);
    // insertAndSetFirstWall(&head, 9,  OVERALL_WINDOW_WIDTH/2+200, OVERALL_WINDOW_HEIGHT/2-200, 10, 300);
    // insertAndSetFirstWall(&head, 10,  OVERALL_WINDOW_WIDTH/2+100, OVERALL_WINDOW_HEIGHT/2-100, 10, 300);
    // insertAndSetFirstWall(&head, 11,  OVERALL_WINDOW_WIDTH/2+100, OVERALL_WINDOW_HEIGHT/2+200, OVERALL_WINDOW_WIDTH/2-100, 10);
    // insertAndSetFirstWall(&head, 12,  OVERALL_WINDOW_WIDTH/2+200, OVERALL_WINDOW_HEIGHT/2+100, OVERALL_WINDOW_WIDTH/2-100, 10);
    

    setup_robot(&robot);
    updateAllWalls(head, renderer);

    SDL_Event event;
    while(!done){
        // SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        // Change background colour
        SDL_SetRenderDrawColor(renderer, 38, 38, 38, 255);
        SDL_RenderClear(renderer);

        //Move robot based on user input commands/auto commands
        if (robot.auto_mode == 1)
            robotAutoMotorMove(&robot, front_left_sensor, front_right_sensor, front_centre_sensor);
        robotMotorMove(&robot);

        // Check if robot reaches endpoint. and check sensor values
        // if (checkRobotReachedEnd(&robot, OVERALL_WINDOW_WIDTH, OVERALL_WINDOW_HEIGHT/2+100, 10, 100)){ // DEFAULT
        // if (checkRobotReachedEnd(&robot, 640, 340, 10, 100)){ //Maze 1
        // if (checkRobotReachedEnd(&robot, 220, 480, 100, 10)){ //Maze 2
        // if (checkRobotReachedEnd(&robot, 0, 340, 10, 100)){ // Maze 3
        // if (checkRobotReachedEnd(&robot, 640-10-320, 480, 100, 10)){ //Maze 4
        // if (checkRobotReachedEnd(&robot, 640, 20, 10, 60)){ //Maze 5
        // if (checkRobotReachedEnd(&robot, 120, 480, 100, 10)){ //Maze 6
        // if (checkRobotReachedEnd(&robot, 0, 20, 10, 60)){ //Maze 7
        if (checkRobotReachedEnd(&robot, 640-10-220, 480, 100, 10)){ //Maze 8   
            // end_time = clock();
            // msec = (end_time-start_time) * 1000 / CLOCKS_PER_SEC;
            gettimeofday(&end_time, 0);
            msec = ((end_time.tv_sec - start_time.tv_sec)*1000)+(end_time.tv_usec - start_time.tv_usec)/1000;
            robotSuccess(&robot, msec);
        }




        // from Ed - Timothy Huxley "Open World"
        // if (checkRobotReachedEnd(&robot, 330, 0, 40, 20)){
        //     end_time = clock();
        //     msec = (end_time-start_time) * 1000 / CLOCKS_PER_SEC;
        //     robotSuccess(&robot, msec);
        // }
        else if(checkRobotHitWalls(&robot, head))
            robotCrash(&robot);
        //Otherwise compute sensor information
        else {


            front_left_sensor = checkRobotSensorFrontLeftAllWalls(&robot, head);
            if (front_left_sensor>0)
                printf("Getting close on the left. Score = %d\n", front_left_sensor);

            front_right_sensor = checkRobotSensorFrontRightAllWalls(&robot, head);
            if (front_right_sensor>0)
                printf("Getting close on the right. Score = %d\n", front_right_sensor);

            front_centre_sensor = checkRobotSensorFrontCentreAllWalls(&robot, head);
            if (front_centre_sensor>0)
                printf("Getting close in the centre. Score = %d\n", front_centre_sensor);
        }
        robotUpdate(renderer, &robot);
        updateAllWalls(head, renderer);


        // Check for user input
        SDL_RenderPresent(renderer);
        while(SDL_PollEvent(&event)){
            if(event.type == SDL_QUIT){
                done = 1;
            }
            const Uint8 *state = SDL_GetKeyboardState(NULL);

            if(state[SDL_SCANCODE_UP] && robot.direction != DOWN){
                robot.direction = UP;
            }
            if(state[SDL_SCANCODE_DOWN] && robot.direction != UP){
                robot.direction = DOWN;
            }
            if(state[SDL_SCANCODE_LEFT] && robot.direction != RIGHT){
                robot.direction = LEFT;
            }
            if(state[SDL_SCANCODE_RIGHT] && robot.direction != LEFT){
                robot.direction = RIGHT;
            }
            if(state[SDL_SCANCODE_SPACE]){
                setup_robot(&robot);
            }
            if(state[SDL_SCANCODE_RETURN]){
                robot.auto_mode = 1;
                //start_time = clock();
                gettimeofday(&start_time, 0);
            }
        }

        SDL_Delay(120);
        // default is 120, reduce it to test in fast-forward mode
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    printf("DEAD\n");
}
