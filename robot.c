#include "robot.h"


// from Ed - Zhuo Chen
// void setup_robot(struct Robot *robot){
//     robot->x = OVERALL_WINDOW_WIDTH/2-300;
//     robot->y = OVERALL_WINDOW_HEIGHT-50;
//     robot->true_x = OVERALL_WINDOW_WIDTH/2-300;
//     robot->true_y = OVERALL_WINDOW_HEIGHT-50;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;
    
//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;
// }

// from Ed - Ehab Ahmed
// void setup_robot(struct Robot *robot){ 
//     robot->x = OVERALL_WINDOW_WIDTH/2-50;
//     robot->y = OVERALL_WINDOW_HEIGHT-50;
//     robot->true_x = OVERALL_WINDOW_WIDTH/2-50;
//     robot->true_y = OVERALL_WINDOW_HEIGHT-50;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;
    
//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
//  }

// from Ed - Gabrielle Zhu
// doesn't technically "succeed" but the end doesn't line up with the finish trigger
// void setup_robot(struct Robot *robot){ 
//     robot->x = OVERALL_WINDOW_WIDTH/6 + 15;
//     robot->y = OVERALL_WINDOW_HEIGHT-50;
//     robot->true_x = OVERALL_WINDOW_WIDTH/6 + 15;
//     robot->true_y = OVERALL_WINDOW_HEIGHT-50;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;
    
//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
//  }

// from Ed - Sumin Baik

// void setup_robot(struct Robot *robot){
// robot->x = OVERALL_WINDOW_WIDTH/5-100;
// robot->y = OVERALL_WINDOW_HEIGHT-50;
// robot->true_x = OVERALL_WINDOW_WIDTH/5-100;
// robot->true_y = OVERALL_WINDOW_HEIGHT-50;
// robot->width = ROBOT_WIDTH;
// robot->height = ROBOT_HEIGHT;
// robot->direction = 0;
// robot->angle = 90;
// robot->currentSpeed = 0;
// robot->crashed = 0;
// robot->auto_mode = 0;
 
//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// from Ed - Justin Klasse
// void setup_robot(struct Robot *robot){
//     robot->x = OVERALL_WINDOW_WIDTH/2-150;
//     robot->y = OVERALL_WINDOW_HEIGHT-50;
//     robot->true_x = OVERALL_WINDOW_WIDTH/2-150;
//     robot->true_y = OVERALL_WINDOW_HEIGHT-50;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

//Gabriella's set up
// void setup_robot(struct Robot *robot){
//    robot->x = OVERALL_WINDOW_WIDTH/2-200;
//    robot->y = OVERALL_WINDOW_HEIGHT-30;
//    robot->true_x = OVERALL_WINDOW_WIDTH/2-200;
//    robot->true_y = OVERALL_WINDOW_HEIGHT-30;
//    robot->width = ROBOT_WIDTH;
//    robot->height = ROBOT_HEIGHT;
//    robot->direction = 0;
//    robot->angle = 0;
//    robot->currentSpeed = 0;
//    robot->crashed = 0;
//    robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//    printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// from Ed - Timothy Huxley "Open World"
// gets to the end ok but clips the corner
  // void setup_robot(struct Robot *robot) {
  //   robot->x = 150;
  //   robot->y = 240;
  //   robot->true_x = 150;
  //   robot->true_y = 240;
  //   robot->width = ROBOT_WIDTH;
  //   robot->height = ROBOT_HEIGHT;
  //   robot->direction = 0;
  //   robot->angle = 180;
  //   robot->currentSpeed = 0;
  //   robot->crashed = 0;
  //   robot->auto_mode = 0;

  //   robot->time = 0;
  //   robot->stuck_time = 0;
  //   robot->stuck = 0;
  //   robot->turn_delay = 0;
  //   robot->right_turn_count = 0;
                          
  //   printf("Press arrow keys to move manually, or enter to move automatically\n\n");
  // }


// //DEFAULT
// void setup_robot(struct Robot *robot){
//     robot->x = OVERALL_WINDOW_WIDTH/2-50;
//     robot->y = OVERALL_WINDOW_HEIGHT-50;
//     robot->true_x = OVERALL_WINDOW_WIDTH/2-50;
//     robot->true_y = OVERALL_WINDOW_HEIGHT-50;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// RACES

// Maze 1
// void setup_robot(struct Robot *robot){
//     robot->x = 270;
//     robot->y = 460;
//     robot->true_x = 270;
//     robot->true_y = 460;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;


//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// Maze 2
// void setup_robot(struct Robot *robot){
//     robot->x = 620;
//     robot->y = 380;
//     robot->true_x = 620;
//     robot->true_y = 380;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 270;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// // Maze 3
// void setup_robot(struct Robot *robot){
//     robot->x = 640-10-270;
//     robot->y = 460;
//     robot->true_x = 640-10-270;
//     robot->true_y = 460;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// Maze 4
// void setup_robot(struct Robot *robot){
//     robot->x = 0;
//     robot->y = 380;
//     robot->true_x = 0;
//     robot->true_y = 380;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 90;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

//Maze 5
// void setup_robot(struct Robot *robot){
//     robot->x = 170;
//     robot->y = 460;
//     robot->true_x = 170;
//     robot->true_y = 460;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

//Maze 6
// void setup_robot(struct Robot *robot){
//     robot->x = 620;
//     robot->y = 40;
//     robot->true_x = 620;
//     robot->true_y = 40;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 270;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

// Maze 7
// void setup_robot(struct Robot *robot){
//     robot->x = 640-10-170;
//     robot->y = 460;
//     robot->true_x = 640-10-170;
//     robot->true_y = 460;
//     robot->width = ROBOT_WIDTH;
//     robot->height = ROBOT_HEIGHT;
//     robot->direction = 0;
//     robot->angle = 0;
//     robot->currentSpeed = 0;
//     robot->crashed = 0;
//     robot->auto_mode = 0;

//     robot->time = 0;
//     robot->stuck_time = 0;
//     robot->stuck = 0;
//     robot->turn_delay = 0;
//     robot->right_turn_count = 0;

//     printf("Press arrow keys to move manually, or enter to move automatically\n\n");
// }

//Maze 8
void setup_robot(struct Robot *robot){
    robot->x = 0;
    robot->y = 40;
    robot->true_x = 0;
    robot->true_y = 40;
    robot->width = ROBOT_WIDTH;
    robot->height = ROBOT_HEIGHT;
    robot->direction = 0;
    robot->angle = 90;
    robot->currentSpeed = 0;
    robot->crashed = 0;
    robot->auto_mode = 0;

    robot->time = 0;
    robot->stuck_time = 0;
    robot->stuck = 0;
    robot->turn_delay = 0;
    robot->right_turn_count = 0;

    printf("Press arrow keys to move manually, or enter to move automatically\n\n");
}

int robot_off_screen(struct Robot * robot){
    if(robot->x < 0 || robot-> y < 0){
        return 0;
    }
    if(robot->x > OVERALL_WINDOW_WIDTH || robot->y > OVERALL_WINDOW_HEIGHT){
        return 0;
    }
    return 1;
}

int checkRobotHitWall(struct Robot * robot, struct Wall * wall) {

    int overlap = checkOverlap(robot->x,robot->width,robot->y,robot->height,
                 wall->x,wall->width,wall->y, wall->height);

    return overlap;
}

int checkRobotHitWalls(struct Robot * robot, struct Wall_collection * head) {
   struct Wall_collection *ptr = head;
   int hit = 0;

   while(ptr != NULL) {
      hit = (hit || checkRobotHitWall(robot, &ptr->wall));
      ptr = ptr->next;
   }
   return hit;

}

int checkRobotReachedEnd(struct Robot * robot, int x, int y, int width, int height){

    int overlap = checkOverlap(robot->x,robot->width,robot->y,robot->height,
                 x,width,y,height);

    return overlap;
}

void robotCrash(struct Robot * robot) {
    robot->currentSpeed = 0;
    if (!robot->crashed)
        printf("Ouchies!!!!!\n\nPress space to start again\n");
    robot->crashed = 1;
}

// void robotSuccess(struct Robot * robot, int msec) {
//     robot->currentSpeed = 0;
//     if (!robot->crashed){
//         printf("Success!!!!!\n\n");
//         printf("Time taken %d seconds %d milliseconds \n", msec/1000, msec%1000);
//         printf("Press space to start again\n");
//     }
//     robot->crashed = 1;
// }

void robotSuccess(struct Robot * robot, unsigned long msec) {
    robot->currentSpeed = 0;
    if (!robot->crashed){
        printf("Success!!!!!\n\n");
        printf("Time taken %lu seconds %lu milliseconds \n", msec/1000, msec%1000);
        printf("Press space to start again\n");
    }
    robot->crashed = 1;
}

int checkRobotSensor(int x, int y, int sensorSensitivityLength, struct Wall * wall)  {
    //viewing_region of sensor is a square of 2 pixels * chosen length of sensitivity
    int overlap = checkOverlap(x,2,y,sensorSensitivityLength,
                 wall->x,wall->width,wall->y, wall->height);
//    int * a;
//
//    a = getGaps(x,2,y,sensorSensitivityLength,
//                 wall->x,wall->width,wall->y, wall->height);
//    printf("%d %d %d %d \n", a[0], a[1], a[2], a[3]);
    return overlap;
}

int checkRobotSensorFrontRightAllWalls(struct Robot * robot, struct Wall_collection * head) {
    struct Wall_collection *ptr, *head_store;
    int i;
    double xDir, yDir;
    int robotCentreX, robotCentreY, xTL, yTL;
    int score, hit;

    int sensorSensitivityLength =  floor(SENSOR_VISION/5);

    head_store = head;
    robotCentreX = robot->x+ROBOT_WIDTH/2;
    robotCentreY = robot->y+ROBOT_HEIGHT/2;
    score = 0;

    for (i = 0; i < 5; i++)
    {
        ptr = head_store;
        xDir = round(robotCentreX+(ROBOT_WIDTH/2 - 10)*cos((robot->angle)*PI/180 + PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*sin((robot->angle)*PI/180 + PI/4));
        yDir = round(robotCentreY+(ROBOT_WIDTH/2 - 10)*sin((robot->angle)*PI/180 + PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*cos((robot->angle)*PI/180 + PI/4));
        xTL = (int) xDir;
        yTL = (int) yDir;
        hit = 0;

        while(ptr != NULL) {
            hit = (hit || checkRobotSensor(xTL, yTL, sensorSensitivityLength, &ptr->wall));
            ptr = ptr->next;
        }
        if (hit)
            score = i;
    }
    return score;
}

int checkRobotSensorFrontLeftAllWalls(struct Robot * robot, struct Wall_collection * head) {
    struct Wall_collection *ptr, *head_store;
    int i;
    double xDir, yDir;
    int robotCentreX, robotCentreY, xTL, yTL;
    int score, hit;
    int sensorSensitivityLength;

    head_store = head;
    robotCentreX = robot->x+ROBOT_WIDTH/2;
    robotCentreY = robot->y+ROBOT_HEIGHT/2;
    score = 0;
    sensorSensitivityLength =  floor(SENSOR_VISION/5);

    for (i = 0; i < 5; i++)
    {
        ptr = head_store;
        xDir = round(robotCentreX+(-ROBOT_WIDTH/2 + 10)*cos((robot->angle)*PI/180 - PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*sin((robot->angle)*PI/180 - PI/4));
        yDir = round(robotCentreY+(-ROBOT_WIDTH/2 + 10)*sin((robot->angle)*PI/180 - PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*cos((robot->angle)*PI/180 - PI/4));
        xTL = (int) xDir;
        yTL = (int) yDir;
        hit = 0;

        while(ptr != NULL) {
            hit = (hit || checkRobotSensor(xTL, yTL, sensorSensitivityLength, &ptr->wall));
            ptr = ptr->next;
        }
        if (hit)
            score = i;
    }
    return score;
}

int checkRobotSensorFrontCentreAllWalls(struct Robot * robot, struct Wall_collection * head) {
    struct Wall_collection *ptr, *head_store;
    int i;
    double xDir, yDir;
    int robotCentreX, robotCentreY, xTL, yTL;
    int score, hit;
    int sensorSensitivityLength =  floor(SENSOR_VISION/5);

    head_store = head;
    robotCentreX = robot->x+ROBOT_WIDTH/2;
    robotCentreY = robot->y+ROBOT_HEIGHT/2;
    score = 0;

    for (i = 0; i < 5; i++) {
        ptr = head_store;
        xDir = round(robotCentreX+(0)*cos((robot->angle)*PI/180)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*sin((robot->angle)*PI/180));
        yDir = round(robotCentreY+(0)*sin((robot->angle)*PI/180)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*cos((robot->angle)*PI/180));
        xTL = (int) xDir;
        yTL = (int) yDir;
        hit = 0;

        while(ptr != NULL) {
            hit = (hit || checkRobotSensor(xTL, yTL, sensorSensitivityLength, &ptr->wall));
            ptr = ptr->next;
        }
        if (hit) {
            score = i;
        }
    }
    return score;
}



//int checkRobotSensorFrontRightSideAllWalls(struct Robot * robot, struct Wall_collection * head) {
//    struct Wall_collection *ptr, *head_store;
//    int i;
//    double xDir, yDir;
//    int robotCentreX, robotCentreY, xTL, yTL;
//    int score, hit;
//
//    int sensorSensitivityLength =  floor(SENSOR_VISION/5);
//
//    head_store = head;
//    robotCentreX = robot->x+ROBOT_WIDTH/2;
//    robotCentreY = robot->y+ROBOT_HEIGHT/2;
//    score = 0;
//
//    for (i = 0; i < 5; i++)
//    {
//        ptr = head_store;
//        xDir = round(robotCentreX+(-ROBOT_WIDTH/2)*cos((robot->angle)*PI/180)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*sin((robot->angle)*PI/180));
//        yDir = round(robotCentreY+(-ROBOT_WIDTH/2)*sin((robot->angle)*PI/180)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*cos((robot->angle)*PI/180));
//        xTL = (int) xDir;
//        yTL = (int) yDir;
//        hit = 0;
//
//        while(ptr != NULL) {
//            hit = (hit || checkRobotSensor(xTL, yTL, sensorSensitivityLength, &ptr->wall));
//            ptr = ptr->next;
//        }
//        if (hit)
//            score = i;
//    }
//    return score;
//}
//
//int checkRobotSensorFrontLeftSideAllWalls(struct Robot * robot, struct Wall_collection * head) {
//    struct Wall_collection *ptr, *head_store;
//    int i;
//    double xDir, yDir;
//    int robotCentreX, robotCentreY, xTL, yTL;
//    int score, hit;
//
//    int sensorSensitivityLength =  floor(SENSOR_VISION/5);
//
//    head_store = head;
//    robotCentreX = robot->x+ROBOT_WIDTH/2;
//    robotCentreY = robot->y+ROBOT_HEIGHT/2;
//    score = 0;
//
//    for (i = 0; i < 5; i++)
//    {
//        ptr = head_store;
//        xDir = round(robotCentreX+(ROBOT_WIDTH/2-2)*cos((robot->angle)*PI/180 - PI/2)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*sin((robot->angle)*PI/180 - PI/2));
//        yDir = round(robotCentreY+(ROBOT_WIDTH/2-2)*sin((robot->angle)*PI/180 - PI/2)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensorSensitivityLength*i)*cos((robot->angle)*PI/180 - PI/2));
//        xTL = (int) xDir;
//        yTL = (int) yDir;
//        hit = 0;
//
//        while(ptr != NULL) {
//            hit = (hit || checkRobotSensor(xTL, yTL, sensorSensitivityLength, &ptr->wall));
//            ptr = ptr->next;
//        }
//        if (hit)
//            score = i;
//    }
//    return score;
//}

void robotUpdate(struct SDL_Renderer * renderer, struct Robot * robot){
    double xDir, yDir;
    double xDirInt, yDirInt;

    int robotCentreX, robotCentreY, xTR, yTR, xTL, yTL, xBR, yBR, xBL, yBL, xTLS, yTLS, xTRS, yTRS;
    SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);


    //Other Display options:
    // The actual square which the robot is tested against (not so nice visually with turns, but easier
    // to test overlap
    SDL_Rect rect = {robot->x, robot->y, robot->height, robot->width};
    // SDL_SetRenderDrawColor(renderer, 80, 80, 80, 255);
    // Change robot square colour
    SDL_SetRenderDrawColor(renderer, 33, 109, 155, 255);
    SDL_RenderDrawRect(renderer, &rect);
    SDL_RenderFillRect(renderer, &rect);

    //Center Line of Robot. Line shows the direction robot is facing
    /*
    xDir = -30 * sin(-robot->angle*PI/180);
    yDir = -30 * cos(-robot->angle*PI/180);
    xDirInt = robot->x+ROBOT_WIDTH/2+ (int) xDir;
    yDirInt = robot->y+ROBOT_HEIGHT/2+ (int) yDir;
    SDL_RenderDrawLine(renderer,robot->x+ROBOT_WIDTH/2, robot->y+ROBOT_HEIGHT/2, xDirInt, yDirInt);
    */

    //Rotating Square
    //Vector rotation to work out corners x2 = x1cos(angle)-y1sin(angle), y2 = x1sin(angle)+y1cos(angle)
    robotCentreX = robot->x+ROBOT_WIDTH/2;
    robotCentreY = robot->y+ROBOT_HEIGHT/2;

    xDir = round(robotCentreX+(ROBOT_WIDTH/2)*cos((robot->angle)*PI/180)-(-ROBOT_HEIGHT/2)*sin((robot->angle)*PI/180));
    yDir = round(robotCentreY+(ROBOT_WIDTH/2)*sin((robot->angle)*PI/180)+(-ROBOT_HEIGHT/2)*cos((robot->angle)*PI/180));
    xTR = (int) xDir;
    yTR = (int) yDir;

    xDir = round(robotCentreX+(ROBOT_WIDTH/2)*cos((robot->angle)*PI/180)-(ROBOT_HEIGHT/2)*sin((robot->angle)*PI/180));
    yDir = round(robotCentreY+(ROBOT_WIDTH/2)*sin((robot->angle)*PI/180)+(ROBOT_HEIGHT/2)*cos((robot->angle)*PI/180));
    xBR = (int) xDir;
    yBR = (int) yDir;

    xDir = round(robotCentreX+(-ROBOT_WIDTH/2)*cos((robot->angle)*PI/180)-(ROBOT_HEIGHT/2)*sin((robot->angle)*PI/180));
    yDir = round(robotCentreY+(-ROBOT_WIDTH/2)*sin((robot->angle)*PI/180)+(ROBOT_HEIGHT/2)*cos((robot->angle)*PI/180));
    xBL = (int) xDir;
    yBL = (int) yDir;

    xDir = round(robotCentreX+(-ROBOT_WIDTH/2)*cos((robot->angle)*PI/180)-(-ROBOT_HEIGHT/2)*sin((robot->angle)*PI/180));
    yDir = round(robotCentreY+(-ROBOT_WIDTH/2)*sin((robot->angle)*PI/180)+(-ROBOT_HEIGHT/2)*cos((robot->angle)*PI/180));
    xTL = (int) xDir;
    yTL = (int) yDir;

    SDL_RenderDrawLine(renderer,xTR, yTR, xBR, yBR);
    SDL_RenderDrawLine(renderer,xBR, yBR, xBL, yBL);
    SDL_RenderDrawLine(renderer,xBL, yBL, xTL, yTL);
    SDL_RenderDrawLine(renderer,xTL, yTL, xTR, yTR);


    int sensor_sensitivity =  floor(SENSOR_VISION/5);
    int i;
    
    //Front Right Sensor

    for (i = 0; i < 5; i++) {
        xDir = round(robotCentreX+(ROBOT_WIDTH/2 - 10)*cos((robot->angle)*PI/180 + PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 + PI/4));
        yDir = round(robotCentreY+(ROBOT_WIDTH/2 - 10)*sin((robot->angle)*PI/180 + PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 + PI/4));
        xTRS = (int) xDir;
        yTRS = (int) yDir;

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawLine(renderer,xTR, yTR, xTRS, yTRS);
    }
    
    //Front Left Sensor
    for (i = 0; i < 5; i++)
    {
        xDir = round(robotCentreX+(-ROBOT_WIDTH/2 + 10)*cos((robot->angle)*PI/180 - PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 - PI/4));
        yDir = round(robotCentreY+(-ROBOT_WIDTH/2 + 10)*sin((robot->angle)*PI/180 - PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 - PI/4));
        xTLS = (int) xDir;
        yTLS = (int) yDir;

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawLine(renderer,xTL, yTL, xTLS, yTLS);

    }

    // Front centre sensor
    for (i = 0; i < 5; i++) {
        xDir = round(robotCentreX+(0)*cos((robot->angle)*PI/180)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180));
        yDir = round(robotCentreY+(0)*sin((robot->angle)*PI/180)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180));
        xTLS = (int) xDir;
        yTLS = (int) yDir;

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawLine(renderer,(xTL+xTR)/2, (yTL+yTR)/2, xTLS, yTLS);
    }

        

    // // Old front centre sensor
    // xDir = -40 * sin(-robot->angle*PI/180);
    // yDir = -40 * cos(-robot->angle*PI/180);
    // xDirInt = robot->x+ROBOT_WIDTH/2+ (int) xDir;
    // yDirInt = robot->y+ROBOT_HEIGHT/2+ (int) yDir;
    // SDL_SetRenderDrawColor(renderer, 80, 80, 80, 255);
    // SDL_RenderDrawLine(renderer,robot->x+ROBOT_WIDTH/2, robot->y+ROBOT_HEIGHT/2, xDirInt, yDirInt);
    

    /* Old sensor drawings

    //Front Right Sensor
    
    int i;
    for (i = 0; i < 5; i++)
    {
        xDir = round(robotCentreX+(ROBOT_WIDTH/2 - 10)*cos((robot->angle)*PI/180 + PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 + PI/4));
        yDir = round(robotCentreY+(ROBOT_WIDTH/2 - 10)*sin((robot->angle)*PI/180 + PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 + PI/4));
        int xTLS = (int) xDir;
        int yTLS = (int) yDir;

        SDL_Rect rect = {xTL, yTL, 2, sensor_sensitivity};
        SDL_SetRenderDrawColor(renderer, 80+(20*(10-i)), 80+(20*(10-i)), 80+(20*(10-i)), 255);
        SDL_RenderDrawRect(renderer, &rect);
        SDL_RenderFillRect(renderer, &rect);
    }

    //Front Left Sensor
    for (i = 0; i < 5; i++)
    {
        xDir = round(robotCentreX+(-ROBOT_WIDTH/2 + 10)*cos((robot->angle)*PI/180 - PI/4)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 - PI/4));
        yDir = round(robotCentreY+(-ROBOT_WIDTH/2 + 10)*sin((robot->angle)*PI/180 - PI/4)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 - PI/4));
        xTL = (int) xDir;
        yTL = (int) yDir;

        SDL_Rect rect = {xTL, yTL, 2, sensor_sensitivity};
        SDL_SetRenderDrawColor(renderer, 80+(20*(10-i)), 80+(20*(10-i)), 80+(20*(10-i)), 255);
        SDL_RenderDrawRect(renderer, &rect);
        SDL_RenderFillRect(renderer, &rect);
    }

    */

    /*//lEFT SIDE SENSOR
    for (i = 0; i < 5; i++)
    {
        xDir = round(robotCentreX+cos((robot->angle)*PI/180 - PI/2)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 - PI/2));
        yDir = round(robotCentreY+sin((robot->angle)*PI/180 - PI/2)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 - PI/2));
        xTL = (int) xDir;
        yTL = (int) yDir;

        SDL_Rect rect = {xTL, yTL, 2, sensor_sensitivity};
        SDL_SetRenderDrawColor(renderer, 80+(20*(5-i)), 80+(20*(5-i)), 80+(20*(5-i)), 255);
        SDL_RenderDrawRect(renderer, &rect);
        SDL_RenderFillRect(renderer, &rect);
    }

    //RIGHT SIDE SENSOR
    for (i = 0; i < 5; i++)
    {
        xDir = round(robotCentreX-cos((robot->angle)*PI/180 + PI/2)-(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*sin((robot->angle)*PI/180 + PI/2));
        yDir = round(robotCentreY-sin((robot->angle)*PI/180 + PI/2)+(-ROBOT_HEIGHT/2-SENSOR_VISION+sensor_sensitivity*i)*cos((robot->angle)*PI/180 + PI/2));
        xTL = (int) xDir;
        yTL = (int) yDir;

        SDL_Rect rect = {xTL, yTL, 2, sensor_sensitivity};
        SDL_SetRenderDrawColor(renderer, 80+(20*(5-i)), 80+(20*(5-i)), 80+(20*(5-i)), 255);
        SDL_RenderDrawRect(renderer, &rect);
        SDL_RenderFillRect(renderer, &rect);
    }*/
}



void robotMotorMove(struct Robot * robot) {
    double x_offset, y_offset;
    switch(robot->direction){
        case UP :
            robot->currentSpeed += DEFAULT_SPEED_CHANGE;
            if (robot->currentSpeed > MAX_ROBOT_SPEED)
                robot->currentSpeed = MAX_ROBOT_SPEED;
            break;
        case DOWN :
            robot->currentSpeed -= DEFAULT_SPEED_CHANGE;
            if (robot->currentSpeed < -MAX_ROBOT_SPEED)
                robot->currentSpeed = -MAX_ROBOT_SPEED;
            break;
        case LEFT :
            robot->angle = (robot->angle+360-DEFAULT_ANGLE_CHANGE)%360;
            break;
        case RIGHT :
            robot->angle = (robot->angle+DEFAULT_ANGLE_CHANGE)%360;
            break;
    }
    robot->direction = 0;
    x_offset = (-robot->currentSpeed * sin(-robot->angle*PI/180));
    y_offset = (-robot->currentSpeed * cos(-robot->angle*PI/180));

    robot->true_x += x_offset;
    robot->true_y += y_offset;

    x_offset = round(robot->true_x);
    y_offset = round(robot->true_y);

    robot->x = (int) x_offset;
    robot->y = (int) y_offset;
}


void robotAutoMotorMove(struct Robot * robot, int front_left_sensor, int front_right_sensor, int front_centre_sensor) {

    // Decision to make - speed (everywhere except right turn)
    // 5 passes all competition mazes, Owen's, Gabriella's
    // 6 - clips walls


    // SETUP / INITIAL CHECKS

    // First, check if stuck - criteria is being still for 9 time increments (might be rotating, but no movement).
    if (robot->currentSpeed==0) {
        robot->stuck_time++;
    } else {
        robot->stuck_time = 0;
    }
    if (robot->stuck_time > 9) {
        robot->stuck = 1;
    }

    // The robot will turn right at 5-increment intervals if no right detected.
    // If we do detect a wall on the right, we don't want to turn any further right, so reset this delay to 0.
    if ((front_right_sensor > 0)) {
        robot->turn_delay = 0;
    }

    // MOVEMENT
    // Two main cases: stuck, or not (the normal case)
    
    // Stuck
    int a = 0;
    int b = 0;
    if (robot->stuck) {
        // Determine the "type" of stuck situation: 
        // a: Walls detected on the centre and at least one other (ie, in a corner)
        // b: any other situatuon where the robot is not moving 

        a = (front_centre_sensor > 0) && ((front_left_sensor > 0) || (front_right_sensor > 0));
        b = (robot->currentSpeed<5);
    }
    if (a) {
        // turn left until sensor condition is no longer met.
        robot->direction = LEFT;
        robot->stuck = 0;
    }
    else if (b) {
        // Move forward for several movements until speed reaches 4, then return to normal control
        robot->direction = UP;
    }

    // Normal case, not stuck
    else {
        robot->stuck = 0;
        
        // If moving forwards at almost full speed and no sensors active.
        if ((robot->currentSpeed == 4) && (front_centre_sensor == 0) && (front_left_sensor == 0) && (front_right_sensor == 0)) {
            // Only turn 165 degrees in a single turn
            if (robot->right_turn_count < 11) {
                // Turn twice every 5 time increments
                if ((robot->time++ % 5 < 2) && (robot->turn_delay > 5)) {
                    robot->direction = RIGHT;
                    robot->right_turn_count++;
                }
            }
        }
        else {
            robot->right_turn_count = 0; 
        
            // Within a narrow section, the side sensors are both active but nothing in front
            // Continue straight
            if ((front_centre_sensor == 0) && (front_left_sensor > 0) && (front_right_sensor > 0)) {
                if (robot->currentSpeed<5){
                    robot->direction = UP;
                }
            }
            // Allows the right sensor to be 1, constantly touching a wall on the right means we can keep going straight
            else if ((front_left_sensor == 0) && (front_right_sensor < 2) && (front_centre_sensor == 0) && (robot->currentSpeed<5)) {
                // Can reach full speed if right is 0 or 1 and others both 0
                robot->direction = UP;
            }
            // Slow down if any sensor detected that doesn't fall into one of the other cases.
            else if ((robot->currentSpeed>0) && ((front_left_sensor > 0) || (front_right_sensor > 0) || (front_centre_sensor > 0))) {
                robot->direction = DOWN;
            } 
            // If stopped with a wall in front, turn left (so that the wall is then on the right)
            else if ((robot->currentSpeed==0) && (front_centre_sensor > 0) && (front_left_sensor > 0) && (front_right_sensor > 0)) {
                robot->direction = LEFT;
            }
            // If a wall is on the left, turn right
            else if ((robot->currentSpeed==0) && ((front_left_sensor > 0) || (front_right_sensor == 0)) ) {
                robot->direction = RIGHT;
            }
            // If a wall is on the right, turn left
            else if ((robot->currentSpeed==0) && ((front_left_sensor == 0) || (front_right_sensor > 0)) ) {
                robot->direction = LEFT;
            }
            // Otherwise, slow down to be safe
            else {
                robot->direction = DOWN;
            }
        }
    }

    // increment the time value and the turn_delay for regularly turning right
    robot->time++;
    robot->turn_delay++;



    // // OLD CODE WITH ONLY TWO SIDE SENSORS

    // // check if stuck
    // if (robot->currentSpeed==0) {
    //     robot->stuck_time++;
    // } else {
    //     robot->stuck_time = 0;
    // }
    // if (robot->stuck_time > 9) {
    //     robot->stuck = 1;
    //     robot->direction = UP;
    //     // fix to go faster, several up movements / until the sensor is 3?
    //     // two execution paths: normal, and getting unstuck.
    //     // up until a sensor reaches 3, then back to normal
    // }
    // if (front_right_sensor > 0) {
    //     robot->turn_delay = 0;
    // }

    // if (robot->stuck) {
    //     if ((front_left_sensor > 2) || (front_right_sensor > 2)) {
    //         robot->stuck = 0;
    //     }
    //     // else if (front_left_sensor > front_right_sensor) {
    //     //     robot->direction = RIGHT;
    //     // }
    //     // else if (front_right_sensor > front_left_sensor) {
    //     //     robot->direction = LEFT;
    //     // }
    //     else if (robot->currentSpeed<4){
    //         robot->direction = UP;
    //     } 
    //     else {
    //         robot->stuck = 0;
    //     }
    // }

    // else {
    //     if ((front_left_sensor == 0) && (front_right_sensor < 2)) {
    //         if ((robot->time++ % 5 < 2) && (front_right_sensor < 1) && (robot->turn_delay > 5)) {
    //             robot->direction = RIGHT;
    //         } 
    //         if (robot->currentSpeed<4){
    //             robot->direction = UP;
    //         }
    //     }
    //     else if ((robot->currentSpeed>0) && ((front_left_sensor > 0) || (front_right_sensor > 0)) ) {
    //         robot->direction = DOWN;
    //     } 
    //     else if ((robot->currentSpeed==0) && ((front_left_sensor > 0) && (front_right_sensor > 0)) ) {
    //         robot->direction = LEFT;
    //     }
    //     else if ((robot->currentSpeed==0) && ((front_left_sensor > 0) || (front_right_sensor == 0)) ) {
    //         robot->direction = RIGHT;
    //     }
    //     else if ((robot->currentSpeed==0) && ((front_left_sensor == 0) || (front_right_sensor > 0)) ) {
    //         robot->direction = LEFT;
    //     }
    // }
    // robot->time++;
    // robot->turn_delay++;
    //printf("current direction, angle, time: %d %d %d\n", robot->direction,robot->angle, robot->time);
    //printf("current speed: %d\n", robot->currentSpeed);
    //printf("stuck for: %d\n", robot->stuck_time);
}
